<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{

	public static function menu() {
        return static::orderBy('priority', 'asc')->get();
	}

	public static function getPreceedingPriority($page) {
		return Page::where('priority', '<', $page->priority)
			->orderBy('priority', 'desc')
			->first();
	}

	public static function decrementPreceedingPriority($page, $previous_priority) {
		return Page::where('priority', '<=', $page->priority)
			->where('id', '<>', $page->id)
			->where('priority', '<>', 0)
			->where('priority', '>', $previous_priority)
			->decrement('priority');
	}

	public static function decrementSucceedingPriority($priority = null) {
		$query = Page::where('priority', '>', $priority);
		return $query->decrement('priority');
	}

	public static function incrementSucceedingPriority($page, $previous_priority = null) {
		$query = Page::where('priority', '>=', $page->priority)
			->where('id', '<>', $page->id);

		if (isset($previous_priority)) {
			$query->where('priority', '<', $previous_priority);
		}

		return $query->increment('priority');
	}

	public function user() {
		return $this->belongsTo('App\User');
	}
}
