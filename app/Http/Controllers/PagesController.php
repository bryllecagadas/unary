<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('garish.admin.pages.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('garish.admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'path' => 'required|unique:pages',
        ]);

        $position = $request->input('position');
        if ($position) {
            $after = Page::find($position);
            $priority = $after->priority + 1;
        } else {
            $priority = 0;
        }

        $page = new Page;
        $page->title = $request->input('title');
        $page->path = '/' . $request->input('path');
        $page->priority = $priority;
        $page->content = $request->input('content');
        $page->user_id = Auth::user()->id;
        $page->save();

        Page::incrementSucceedingPriority($page);

        $message = 'Page Created';
        if ($request->input('generate') && $this->generate($page, FALSE)) {
            $message = "Page created & generated";
        }

        return redirect('/admin/pages/' . $page->id)->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = Page::find($id);
        return view('garish.admin.pages.show', ['page' => $page]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::find($id);

        $page->path = substr($page->path, 1);

        $before = Page::getPreceedingPriority($page);
        return view('garish.admin.pages.edit', ['page' => $page, 'before' => $before]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page = Page::find($id);

        $options = [
            'title' => 'required',
            'path' => 'required|unique:pages',
        ];

        $path = $request->input('path');
        if ($page->path == '/' && !$path) {
            unset($options['path']);
            $path = '/';
        } else {
            $path = '/' . $path;
        }

        $decrement_preceeding = $increment_succeeding = FALSE;
        $position = $request->input('position');
        $previous_priority = $priority = $page->priority;

        if ($position) {
            $after = Page::find($position);
            if ($after->priority > $page->priority) {
                $priority = $after->priority;
                $decrement_preceeding = TRUE;
            } else if ($after->priority < $page->priority) {
                $priority = $after->priority + 1;
                $decrement_preceeding = TRUE;
                $increment_succeeding = TRUE;
            }
        } else {
            $priority = 0;
            $increment_succeeding = TRUE;
        }

        $this->validate($request, $options);

        $page->title = $request->input('title');
        $page->path = $path;
        $page->priority = $priority;
        $page->content = $request->input('content');
        $page->save();

        if ($decrement_preceeding) {
            Page::decrementPreceedingPriority($page, $previous_priority);
        }

        if ($increment_succeeding) {
            Page::incrementSucceedingPriority($page, $previous_priority);
        }

        $message = 'Page Updated';
        if ($request->input('generate') && $this->generate($page, FALSE)) {
            $message = "Page updated & generated";
        }

        return redirect('/admin/pages/' . $page->id)->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::find($id);

        if ($page->path == '/') {
            return redirect('/admin/pages')->with('error', 'Cannot delete home page.');
        }

        $priority = $page->priority;
        Page::decrementSucceedingPriority($priority);
        $page->delete();

        $this->removeGenerated($id);

        return redirect('/admin/pages')->with('success', 'Page Deleted');
    }

    public function preview($id) {
        $page = Page::find($id);

        return view('garish.base.pages.show', [
            'page' => $page, 
            'generated_path' => $page->path,
        ]);
    }

    public function generate(Page $page, $redirect = TRUE) {
        $printer = app('App\Printers\WebPrint');

        $contents = view('garish.base.pages.show', [
            'page' => $page, 
            'generated_path' => $page->path,
        ]);

        $result = $printer->render((string) $contents, $page->path);

        if (!$redirect) {
            return $result;
        }

        if ($result) {
            $args = array('success' => 'Page Generated');
        } else {
            $args = array('error' => 'There was an issue generating the page');
        }

        return redirect(request()->headers->get('referer'))->with($args);
    }

    public function removeGenerated($id) {
        $page = Page::find($id);
    }
}
