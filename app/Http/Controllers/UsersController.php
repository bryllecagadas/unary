<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('garish.admin.users.index', ['users' => $this->getUsers()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('garish.admin.users.create', ['users' => $this->getUsers()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|string|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'role' => ['required', Rule::in(array_keys(roles_list()))],
        ]);

        $user = new User;
        $user->username = $request->input('username');
        $user->password = bcrypt($request->input('password'));
        $user->role = $request->input('role');
        $user->save();

        return redirect('/admin/users/' . $user->id)->with('success', 'User Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('garish.admin.users.show', ['user' => $user, 'users' => $this->getUsers()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('garish.admin.users.edit', ['user' => $user, 'users' => $this->getUsers()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $rules = [
            'role' => ['required', Rule::in(array_keys(roles_list()))],
        ];

        $username = $request->input('username');
        $password = $request->input('password');

        if ($username != $user->username) {
            $rules['username'] = 'required|string|unique:users';
        }
        
        if ($password) {
            $rules['password'] = 'string|min:6|confirmed';
        }
        
        $this->validate($request, $rules);

        
        $user->username = $username;
        $user->role = $request->input('role');

        if ($password) {
            $user->password = bcrypt($password);
        }

        $user->save();

        return redirect('/admin/users/' . $user->id)->with('success', 'User Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($id == 1) {
            return redirect('/admin/users')->with('error', 'Admin can\'t be deleted');
        }

        $user = User::find($id);
        $user->delete();
        return redirect('/admin/users')->with('success', 'User Deleted');
    }

    public function getUsers() {
        $users = User::orderBy('role', 'asc')->orderBy('created_at', 'asc')->get();
        return $users;
    }
}
