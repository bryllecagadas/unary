<?php

namespace App\Http\Controllers;

use App\Page;
use App\Post;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class AdminController  extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard() {
        return view('garish.admin.dashboard', ['messages' => $this->messages()]);
    }

    public function messages() {
        $raw_values = [
            'user_count' => User::count(),
            'post_count' => Post::count(),
        ];

        $messages = [
            'user_count' => [], 
            'post_count' => []
        ];

        foreach ($messages as $index => &$props) {
            $verb = $raw_values[$index] == 1 ? 'is' : 'are';
            if ($index == 'user_count') {
                $noun = str_plural('user', $raw_values[$index]);
                $props = [
                    'message' => "There $verb currently %s",
                    'raw_values' => [$raw_values['user_count']],
                    'values' => [
                        html_link($raw_values['user_count'] . " $noun", 'admin/users')
                    ],
                ];
            } else if ($index == 'post_count') {
                $noun = str_plural('post', $raw_values[$index]);
                $props = [
                    'message' => "There $verb currently %s",
                    'raw_values' => [$raw_values['post_count']],
                    'values' => [
                        html_link(Post::count() . " $noun", 'admin/posts')
                    ],
                ];
            }
        }

        return $messages;
    }
}
