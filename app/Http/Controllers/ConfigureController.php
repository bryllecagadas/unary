<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConfigureController extends Controller
{
	public function __construct() {
		$this->middleware('auth');
	}

    public function index() {
    	return view('garish.admin.configure.index');
    }
}
