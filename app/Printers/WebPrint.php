<?php

namespace App\Printers;

class WebPrint {
	protected $basepath;

	public function __construct($base = '') {
		$this->basepath = $base ? $base : public_path();
	}

	public function remove($path) {
		unlink($path);
	}

	public function render($contents, $path, $options = array()) {
		// Configure options
		$defaults = array(
			'basepath' => $this->basepath
		);

		$options = array_merge($defaults, $options);

		// Clean up target path
		$basepath = $options['basepath'];
		$target_path = $basepath . $path;

		if (substr($target_path, -1) === '/') {
			$target_path = substr($target_path, 0, -1);
		}

		// Determine actual file name
		$pathinfo = pathinfo($target_path);
		if (strpos($pathinfo['filename'], '.') === FALSE) {
			$target_path .= '/' . 'index.html';
			$pathinfo = pathinfo($target_path);
		}

		if (!file_exists($pathinfo['dirname'])) {
			mkdir($pathinfo['dirname'], 0764, TRUE);
		}

		return file_put_contents($target_path, $contents);
	}
}