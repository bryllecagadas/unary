<?php

use Illuminate\Support\Str;

if (!function_exists('user_link')) {
	function user_link($user, $options = []) {

		$defaults = [
			'prefix' => '',
			'title' => 'username',
		];

		$options = array_merge($defaults, $options);

		if ($options['prefix'] && substr($options['prefix'], -1) !== '/') {
			$options['prefix'] .= '/';
		}

		$title_source = $options['title'];

		$title = isset($user->$title_source) ? $user->$title_source : $user->username;
		$url = url($options['prefix'] . 'user/' . $user->id);
		return "<a href='$url' title='$title'>$title</a>";
	}
}

if (!function_exists('post_date')) {
	function post_date($date_string, $options = []) {
		$time = strtotime($date_string);

		$defaults = [
			'format' => 'F j, Y - h:i A'
		];

		$options = array_merge($defaults, $options);

		if ($options['format'] == $defaults['format'] && date("Y") == date("Y", $time)) {
			$options['format'] = 'F j - h:i A';
		}

		$format = $options['format'];
		return date($format, $time);
	}
}

if (!function_exists('roles_list')) {
	function roles_list() {
		return array(
			'admin' => 'Administrator',
			'editor' => 'Editor',
			'user' => 'User',
		);
	}
}

if (!function_exists('format_role')) {
	function format_role($role) {
		$roles = roles_list();
		return isset($roles[$role]) ? $roles[$role] : $role;
	}
}

if (!function_exists('html_link')) {
	function html_link($text, $path, $parameters = [], $secure = NULL) {
		$url = url($path, $parameters, $secure);
		$attribute_str = "";

		$args = [$url];
		if (isset($parameters['attributes'])) {
			foreach ($parameters['attributes'] as $attribute => $value) {
				$attribute_str .= " $attribute='%s'";
				$args[] = $value;
			}
		}

		array_unshift($args, "<a href='%s'$attribute_str>%s</a>");
		$args[] = $text;
		return call_user_func_array('sprintf', $args);
	}
}

if (!function_exists('is_url_active')) {
	function is_url_active($path, $to_check) {
		if ($path == '/') {
			return $path == $to_check;
		}
		return Str::is($path . '*', $to_check);
	}
}