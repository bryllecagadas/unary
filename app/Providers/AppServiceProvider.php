<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use \App\Printers\WebPrint;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer([
            'garish.base.inc.header', 
            'garish.admin.pages.list',
            'garish.admin.pages.edit',
            'garish.admin.pages.create',
        ], function($view) {
            $view->with('pages', \App\Page::menu());
        });

        view()->composer(['garish.admin.posts.list'], function($view) {
            $view->with('posts', \App\Post::recent());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(WebPrint::class, function() {
            return new WebPrint();
        });
    }
}
