<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function user() {
    	return $this->belongsTo('App\User');
    }

    public static function relativePath($post) {
    	return '/blog' . $post->path;
    }

	public static function recent() {
        return static::orderBy('created_at', 'desc')->get();
	}
}
