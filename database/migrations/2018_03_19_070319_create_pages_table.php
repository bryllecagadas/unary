<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('priority')->default(0);
            $table->integer('user_id')->default(0);
            $table->string('path')->default('')->unique();
            $table->string('title');
            $table->text('content')->nullable();
            $table->timestamps();
        });

        $page = new App\Page();
        $page->user_id = 1;
        $page->priority = 0;
        $page->path = '/';
        $page->title = 'Home';
        $page->content = '';
        $page->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
