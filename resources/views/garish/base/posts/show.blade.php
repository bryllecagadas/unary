@extends('garish.layouts.base')
@section('title', $post->title . ' on Unary')
@section('content')
	<div class="post-wrapper">
		<h2>{{$post->title}}</h2>
		<div class="author">
			by <span class="name">{!!user_link($post->user)!!}</span> <span class="date">{{$post->created_at->diffForHumans()}}</span>
		</div>
		<div class="body">
			{!!$post->content!!}
			<p class="center-text">- End -</p>
		</div>
	</div>
@endsection
@section('comments')
	<div id="comments">
		<h3>Comments</h3>
		<div class="comment row">
			<div class="two columns">
				<div class="avatar"></div>
			</div>
			<div class="details ten columns">
				<div class="author">
					<div class="name">Someone Named Chuck</div>
					<div class="title"><span class="title">Re: Digitally Static</span> on <span class="date">April 17, 2016</span></div>
				</div>
				<div class="body">
					<p>How does a chronic procrastinator do it?</p>
				</div>
			</div>
			<div class="clearboth"></div>
		</div>
	</div>
@endsection