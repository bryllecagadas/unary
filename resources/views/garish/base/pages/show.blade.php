@extends('garish.layouts.base')
@section('title', 'Unary | ' . $page->title)
@section('content')
	<div class="post-wrapper">
		<h2>{{$page->title}}</h2>
		<div class="body">
			{!!$page->content!!}
		</div>
	</div>
@endsection