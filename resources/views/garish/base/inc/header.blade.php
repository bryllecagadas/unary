<header>
	<div class="container">
		<h1 class="logo">
			<a href="{{url('/')}}"><img src="{{asset('img/logo-v2-200w.png')}}" alt="Unary" title="Unary" /></a>
		</h1>
		<nav id="nav">
			<ul class="menu">
				@foreach($pages as $single_page)
				<li class="{{is_url_active($single_page->path, $generated_path) ? ' active' : ''}}">
					<a href="{{url($single_page->path)}}">{{$single_page->title}}</a>
				</li>
				@endforeach
			</ul>
		</nav>
		<div class="clearboth"></div>
	</div>
</header>