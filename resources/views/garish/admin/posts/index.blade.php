@extends('garish.layouts.admin')
@section('title', 'Post List')
@section('panes')
	@include('garish.admin.posts.list')
@endsection