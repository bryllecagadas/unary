@extends('garish.layouts.admin')
@section('title', $post->title)
@section('panes')
	@include('garish.admin.posts.list')
	<div class="pane main">
		<div class="pane-header">
			Post: {{$post->title}}
		</div>
		<div class="pane-content">
			<div class="content">
				<div class="header">
					<h1>{{$post->title}}</h1>
					@if($post->user)
						<small>by {!!user_link($post->user, ['prefix' => 'admin'])!!} on {{post_date($post->created_at)}}</small>
					@else
						<small>created on {{post_date($post->created_at)}}</small>
					@endif
				</div>
				<div>
					{!!$post->content!!}
				</div>
				<div class="actions">
					<a href="/admin/posts/{{$post->id}}/preview" class="button" target="_blank">
						<i class="fas fa-eye"></i> Preview
					</a>
					<a href="/admin/posts/{{$post->id}}/edit" class="button"><i class="fas fa-edit"></i> Edit</a>
					{!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST'])!!}
						{{Form::hidden('_method', 'DELETE')}}
						{!!Form::button('<i class="fas fa-trash"></i> Delete', ['class' => 'warning', 'type' => 'submit'])!!}
					{!!Form::close()!!}
				</div>
			</div>
		</div>
	</div>
@endsection