@extends('garish.layouts.admin')
@section('title', 'Add Post')
@section('panes')
	@include('garish.admin.posts.list')
	<div class="pane main">
		<div class="pane-header">
			@yield('title')
		</div>
		<div class="pane-content">
			<div class="content">
				{!! Form::open(['action' => 'PostsController@store', 'method' => 'POST']) !!}
					<div class="row">
						{{Form::label('title', 'Title')}}
						{{Form::text('title', '', ['placeholder' => 'Title'])}}
					</div>
					<div class="row">
						{{Form::label('content', 'Content')}}
						{{Form::textarea('content', '', ['id' => 'article-ckeditor'])}}
					</div>
					<div class="actions">
						{!!Form::button('<i class="fas fa-save"></i> Add', ['type' => 'submit'])!!}
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection