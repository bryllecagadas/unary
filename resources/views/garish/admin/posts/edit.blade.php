@extends('garish.layouts.admin')
@section('title', $post->title)
@section('panes')
	@include('garish.admin.posts.list')
	<div class="pane main">
		<div class="pane-header">
			Edit Post: {{$post->title}}
		</div>
		<div class="pane-content">
			<div class="content">
				{!! Form::open(['action' => ['PostsController@update', $post->id], 'method' => 'POST']) !!}
					<div class="row">
						{{Form::label('title', 'Title')}}
						{{Form::text('title', $post->title, ['placeholder' => 'Title'])}}
					</div>
					<div class="row">
						{{Form::label('content', 'Content')}}
						{{Form::textarea('content', $post->content, ['id' => 'article-ckeditor'])}}
					</div>
					{{Form::hidden('_method', 'PUT')}}
					<div class="actions">
						{!!Form::button('<i class="fas fa-save"></i> Save', ['type' => 'submit'])!!}
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection