<div class="pane">
	<div class="pane-header">
		Posts
	</div>
	<div class="pane-content">
		<ul class="selectable-items">
			<li class="selectable-item selectable-item-action {{Request::is('admin/posts/create') ? 'active' : ''}}">
				<a href="{{url('admin/posts/create')}}" class="action">
					<small>Add Post <i class="fas fa-plus"></i></small>
				</a>
			</li>
			@if(count($posts) > 0)
				@foreach($posts as $single_post)
					<li class="selectable-item {{Request::is('admin/posts/' . $single_post->id) || Request::is('admin/posts/' . $single_post->id . '/*') ? 'active' : ''}}">
						<a href="{{url('admin/posts/' . $single_post->id)}}">
							<h2>{{$single_post->title}}</h2>
							@if($single_post->user)
								<small>by {{$single_post->user->username}} on {{post_date($single_post->created_at)}}</small>
							@else
								<small>created on {{post_date($single_post->created_at)}}</small>
							@endif
						</a>
					</li>
				@endforeach
			@endif
		</ul>
	</div>
</div>