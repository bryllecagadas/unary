@extends('garish.layouts.admin')
@section('title', 'Users List')
@section('panes')
	@include('garish.admin.users.list')
@endsection