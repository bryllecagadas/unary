<div class="pane">
	<div class="pane-header">
		Users
	</div>
	<div class="pane-content">
		<ul class="selectable-items">
			<li class="selectable-item selectable-item-action {{Request::is('admin/users/create') ? 'active' : ''}}">
				<a href="{{url('admin/users/create')}}" class="action">
					<small>Add User <i class="fas fa-plus"></i></small>
				</a>
			</li>
			@if(count($users) > 0)
				@foreach($users as $single_user)
					<li class="selectable-item {{Request::is('admin/users/' . $single_user->id) || Request::is('admin/users/' . $single_user->id . '/*') ? 'active' : ''}}">
						<a href="{{url('admin/users/' . $single_user->id)}}">
							<h2>{{$single_user->username}}</h2>
							<small>{{format_role($single_user->role)}}</small>
						</a>
					</li>
				@endforeach
			@endif
		</ul>
	</div>
</div>