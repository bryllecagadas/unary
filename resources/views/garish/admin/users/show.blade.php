@extends('garish.layouts.admin')
@section('title', 'User: ' . $user->username)
@section('panes')
	@include('garish.admin.users.list')
	<div class="pane main">
		<div class="pane-header">
			User: {{$user->username}}
		</div>
		<div class="pane-content">
			<div class="content">
				<div class="header">
					<h1>{{$user->username}}</h1>
					<small>{{format_role($user->role)}}</small>
				</div>
				<div class="actions">
					<a href="/admin/users/{{$user->id}}/edit" class="button"><i class="fas fa-edit"></i> Edit</a>
					{!!Form::open(['action' => ['UsersController@destroy', $user->id], 'method' => 'POST'])!!}
						{{Form::hidden('_method', 'DELETE')}}
						{!!Form::button('<i class="fas fa-trash"></i> Delete', ['class' => 'warning', 'type' => 'submit'])!!}
					{!!Form::close()!!}
				</div>
			</div>
		</div>
	</div>
@endsection