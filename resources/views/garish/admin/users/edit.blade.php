@extends('garish.layouts.admin')
@section('title', 'Edit User')
@section('panes')
	@include('garish.admin.users.list')
	<div class="pane main">
		<div class="pane-header">
			Edit User: {{$user->username}}
		</div>
		<div class="pane-content">
			<div class="content">
				{!! Form::open(['action' => ['UsersController@update', $user->id], 'method' => 'POST']) !!}
					<div class="row">
						{{Form::label('username', 'Username')}}
						{{Form::text('username', $user->username, ['placeholder' => 'Username', 'autocomplete' => 'off'])}}
						<span class="ui small">Must be unique</span>
					</div>
					<div class="row">
						{{Form::label('password', 'Password')}}
						{{Form::password('password', '', ['placeholder' => 'Password', 'autocomplete' => 'off'])}}
					</div>
					<div class="row">
						{{Form::label('password_confirmation', 'Password Confirm')}}
						{{Form::password('password_confirmation', '', ['placeholder' => 'Password', 'autocomplete' => 'off'])}}
					</div>
					<div class="row">
						{{Form::label('role', 'Role')}}
						{{Form::select('role', roles_list(), $user->role)}}
					</div>
					{{Form::hidden('_method', 'PUT')}}
					<div class="actions">
						{!!Form::button('<i class="fas fa-save"></i> Save', ['type' => 'submit'])!!}
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection