<div class="pane">
	<div class="pane-header">
		Settings
	</div>
	<div class="pane-content">
		<ul class="selectable-items">
			<li class="selectable-item selectable-item-action {{Request::is('admin/configure/pages') ? 'active' : ''}}">
				<a href="{{url('admin/configure/pages')}}">
					<h2>Pages</h2>
				</a>
			</li>
			<li class="selectable-item selectable-item-action {{Request::is('admin/configure/posts') ? 'active' : ''}}">
				<a href="{{url('admin/configure/posts')}}">
					<h2>Posts</h2>
				</a>
			</li>
			<li class="selectable-item selectable-item-action {{Request::is('admin/configure/system') ? 'active' : ''}}">
				<a href="{{url('admin/configure/system')}}">
					<h2>System</h2>
				</a>
			</li>
		</ul>
	</div>
</div>