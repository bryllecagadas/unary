@extends('garish.layouts.admin')
@section('title', 'Login')
@section('window-class', 'float')
@section('content')
<div class="login-dialog">
	<div class="dialog">
		<div class="row center-text">
			<a href="{{url('/')}}" class="logo"><img src="{{asset('img/logo-v2-invert-135w.png')}}" alt="Unary" title="Unary" /></a>
		</div>
		{!!Form::open(['url' => route('login'), 'method' => 'POST'])!!}
            {{ csrf_field() }}
			<div class="row">
				<div class="five columns right-text">{{Form::label('username', 'Username')}}</div>
				<div class="five columns">{{Form::text('username', '', ['autofocus', 'class' => 'u-full-width', 'placeholder' => 'USERNAME'])}}</div>
			</div>
			<div class="row">
				<div class="five columns right-text">{{Form::label('password', 'Password')}}</div>
				<div class="five columns">{{Form::password('password', ['class' => 'u-full-width', 'placeholder' => 'PASSWORD'])}}</div>
			</div>
			<div class="row center-text">
				{{Form::button('Login', ['type' => 'submit'])}}
			</div>
			<div class="row center-text">
				<a class="ui" href="#"><small>Forgot Password</small></a>
			</div>
		{!!Form::close()!!}
	</div>
</div>
@endsection