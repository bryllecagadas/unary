<div class="pane-header">
	<a href="{{url('/')}}" class="logo"><img src="{{asset('img/logo-v2-invert-135w.png')}}" alt="Unary" title="Unary" /></a>
</div>
<div class="pane-content">
	<div class="sidebar">
		<ul class="nav-bar">
			<li class="{{Request::is('admin') ? 'active' : ''}}"><a href="{{url('/admin')}}"><i class="fas fa-home"></i> Dashboard</a></li>
			<li class="{{Request::is('admin/pages*') ? 'active' : ''}}"><a href="{{url('/admin/pages')}}"><i class="fas fa-file-alt"></i> Pages</a></li>
			<li class="{{Request::is('admin/posts*') ? 'active' : ''}}"><a href="{{url('/admin/posts')}}"><i class="far fa-newspaper"></i> Posts</a></li>
			<li class="{{Request::is('admin/users*') ? 'active' : ''}}"><a href="{{url('/admin/users')}}"><i class="fas fa-user"></i> Users</a></li>
			<li class="{{Request::is('admin/configure*') ? 'active' : ''}}"><a href="{{url('/admin/configure')}}"><i class="fas fa-cog"></i> Configure</a></li>
			<li><a href="{{ route('logout') }}"  onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i> Logout</a></li>
		</ul>		
	    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	        {{ csrf_field() }}
	    </form>
	</div>
</div>