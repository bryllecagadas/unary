@extends('garish.layouts.admin')
@section('title', ucwords(Auth::user()->username . '\'s Dashboard'))
@section('panes')
	<div class="pane">
		<div class="pane-header">
			@yield('title')
		</div>
		<div class="pane-content">
			<div class="content">
				<h1>Welcome to Unary</h1>
				<div class="row">
					<small>Today is {{date("F j, Y")}}</small>
				</div>
				<hr />
				@foreach($messages as $type => $props)
					<div class="row">
						{!!call_user_func_array(
							'sprintf', 
							array_merge(
								[$props['message']], 
								$props['values']
							)
						)!!}
					</div>
				@endforeach
			</div>
		</div>
	</div>
@endsection