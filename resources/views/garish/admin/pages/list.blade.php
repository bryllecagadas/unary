<div class="pane">
	<div class="pane-header">
		Pages
	</div>
	<div class="pane-content">
		<ul class="selectable-items">
			<li class="selectable-item selectable-item-action {{Request::is('admin/pages/create') ? 'active' : ''}}">
				<a href="{{url('admin/pages/create')}}" class="action">
					<small>Add Page <i class="fas fa-plus"></i></small>
				</a>
			</li>
			@if(count($pages) > 0)
				@foreach($pages as $single_page)
					<li class="selectable-item {{Request::is('admin/pages/' . $single_page->id) || Request::is('admin/pages/' . $single_page->id . '/*') ? 'active' : ''}}">
						<a href="{{url('admin/pages/' . $single_page->id)}}">
							<h2>{{$single_page->title}}</h2>
							@if($single_page->user)
								<small>by {{$single_page->user->username}} on {{post_date($single_page->created_at)}}</small>
							@else
								<small>created on {{post_date($single_page->created_at)}}</small>
							@endif
						</a>
					</li>
				@endforeach
			@endif
		</ul>
	</div>
</div>