@extends('garish.layouts.admin')
@section('title', $page->title)
@section('panes')
	@include('garish.admin.pages.list')
	<div class="pane main">
		<div class="pane-header">
			Page: {{$page->title}}
		</div>
		<div class="pane-content">
			<div class="content">
				<div class="header">
					<h1>{{$page->title}}</h1>
					@if($page->user)
						<small>by {!!user_link($page->user, ['prefix' => 'admin'])!!} on {{post_date($page->created_at)}}</small>
					@else
						<small>created on {{post_date($page->created_at)}}</small>
					@endif
					<div><small>Path: {{$page->path}}</small></div>
				</div>
				<div class="row">
					{!!$page->content!!}
				</div>
				<div class="actions">
					<a href="/admin/pages/{{$page->id}}/preview" class="button" target="_blank">
						<i class="fas fa-eye"></i> Preview
					</a>
					<a href="/admin/pages/{{$page->id}}/generate" class="button">
						<i class="fas fa-sync-alt"></i> Re-Generate
					</a>
					<a href="/admin/pages/{{$page->id}}/edit" class="button"><i class="fas fa-edit"></i> Edit</a>
					{!!Form::open(['action' => ['PagesController@destroy', $page->id], 'method' => 'POST'])!!}
						{{Form::hidden('_method', 'DELETE')}}
						{!!Form::button('<i class="fas fa-trash-alt"></i> Delete', ['class' => 'warning', 'type' => 'submit'])!!}
					{!!Form::close()!!}
				</div>
			</div>
		</div>
	</div>
@endsection