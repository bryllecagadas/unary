@extends('garish.layouts.admin')
@section('title', $page->title)
@section('panes')
	@include('garish.admin.pages.list')
	<div class="pane main">
		<div class="pane-header">
			Edit Page: {{$page->title}}
		</div>
		<div class="pane-content">
			<div class="content">
				{!! Form::open(['action' => ['PagesController@update', $page->id], 'method' => 'POST']) !!}
					<div class="row">
						{{Form::label('title', 'Title')}}
						{{Form::text('title', $page->title, ['placeholder' => 'Title'])}}
					</div>
					<div class="row">
						{{Form::label('path', 'Path')}}
						<small class="ui">/</small>{{Form::text('path', $page->path, ['placeholder' => 'Path'])}}
					</div>
					<div class="row">
						{{Form::label('position', 'Position')}}
						{{Form::select(
							'position', 
							$pages->pluck('title', 'id')
								->except([$page->id])
								->map(function($item, $key) {
									return 'After ' . $item;
								}), 
							$before ? $before->id : '', 
							['placeholder' => 'Beginning']
						)}}
					</div>
					<div class="row">
						{{Form::label('content', 'Content')}}
						{{Form::textarea('content', $page->content, ['id' => 'article-ckeditor'])}}
					</div>
					<div class="row">
						<label>
							{{Form::checkbox('generate', '1', true)}}
							<span class="label-body">Generate after saving</span>
						</label>
					</div>
					{{Form::hidden('_method', 'PUT')}}
					<div class="actions">
						{!!Form::button('<i class="fas fa-save"></i> Save', ['type' => 'submit'])!!}
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection