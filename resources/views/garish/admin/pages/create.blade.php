@extends('garish.layouts.admin')
@section('title', 'Add Page')
@section('panes')
	@include('garish.admin.pages.list')
	<div class="pane main">
		<div class="pane-header">
			@yield('title')
		</div>
		<div class="pane-content">
			<div class="content">
				{!! Form::open(['action' => 'PagesController@store', 'method' => 'POST']) !!}
					<div class="row">
						{{Form::label('title', 'Title')}}
						{{Form::text('title', '', ['placeholder' => 'Title'])}}
					</div>
					<div class="row">
						{{Form::label('path', 'Path')}}
						<small class="ui">/</small>{{Form::text('path', '', ['placeholder' => 'Path'])}}
					</div>
					<div class="row">
						{{Form::label('position', 'Position')}}
						{{Form::select(
							'position', 
							$pages->pluck('title', 'id')
								->map(function($item, $key) {
									return 'After ' . $item;
								}), 
							'', 
							['placeholder' => 'Beginning']
						)}}
					</div>
					<div class="row">
						{{Form::label('content', 'Content')}}
						{{Form::textarea('content', '', ['id' => 'article-ckeditor'])}}
					</div>
					<div class="row">
						{{Form::checkbox('generate', '1')}}
						<span class="label-body">Generate after saving</span>
					</div>
					<div class="actions">
						{!!Form::button('<i class="fas fa-save"></i> Add', ['type' => 'submit'])!!}
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection