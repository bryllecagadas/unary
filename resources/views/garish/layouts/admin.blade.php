<!doctype html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="{{asset('css/normalize.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/skeleton.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/skeleton-alerts.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/garish/style.css')}}">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Unary | @yield('title')</title>
</head>
<body>
	<div class="row">@include('garish.messages')</div>
	<div class="vert-center">
		<div class="window @yield('window-class')">
			@if (empty($hideNavigation))
				<div class="pane pane-invert pane-sidebar">
					@include('garish.admin.inc.sidebar')
				</div>
			@endif
			@hasSection('content')
				@yield('content')
			@endif
			@yield('panes')
		</div>
	</div>
	@include('garish.admin.inc.footer')
</body>
</html>