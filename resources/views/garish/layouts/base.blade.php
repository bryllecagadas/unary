<!doctype html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="{{asset('css/normalize.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/skeleton.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/skeleton-alerts.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/garish/style.css')}}">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>@yield('title')</title>
</head>
<body>
	@include('garish.base.inc.header')
	<div class="container">
		<div id="content">
			@yield('content')
		</div>
		@yield('comments')
	</div>
	@include('garish.base.inc.footer')
</body>
</html>